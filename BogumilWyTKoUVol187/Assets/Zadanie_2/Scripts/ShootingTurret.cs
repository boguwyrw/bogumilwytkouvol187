﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ShootingTurret : MonoBehaviour
{
    private float randomAngle;
    private float delayTime = 0.5f;
    private float timeToRotate;

    private void Start()
    {
        timeToRotate = delayTime;
        InvokeRepeating("RotatingShootingTurret", 0, delayTime); // 0 - czas od startu w jakim ma zacząć działać funkcja, 3 - co ile sekund ma działać funkcja
    }

    private void Update()
    {
        //RotatingShootingTurret();
    }

    private void RotatingShootingTurret()
    {
        randomAngle = Random.Range(15.0f, 45.0f);
        transform.Rotate(0, 0, randomAngle);
        /*
        timeToRotate = timeToRotate - Time.deltaTime;
        if (timeToRotate <= 0.0f)
        {
            randomAngle = Random.Range(15.0f, 45.0f);
            transform.Rotate(0, 0, randomAngle);
            timeToRotate = delayTime;
        }
        */
    }
}
