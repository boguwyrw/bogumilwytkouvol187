﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabBall : MonoBehaviour
{
    private Vector3 mousePosition;
    private RaycastHit2D raycastHit;

    private void Update()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        raycastHit = Physics2D.Raycast(new Vector2(mousePosition.x, mousePosition.y), Vector2.zero, 0);

        if ((raycastHit.collider != null) && Input.GetMouseButton(0))
        {
            raycastHit.collider.transform.position = new Vector2(mousePosition.x, mousePosition.y);
        }

    }

}
